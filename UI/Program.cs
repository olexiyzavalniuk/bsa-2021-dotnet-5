﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using UI.DTO;

namespace UI
{
    class Program
    {
        public const string path = "https://localhost:44322/api/";
        public static HttpClient client = new HttpClient();
        static void Main(string[] args)
        {           
            Console.WriteLine(" This is console interface. You can use command ");
            Console.WriteLine(" \"man\" to see list of available commands. And ");
            Console.WriteLine(" \"quit\" to exit from program ");
            Console.WriteLine();

            bool flag = true;
            while (flag)
            {
                Console.Write(">");
                string command = Console.ReadLine();
                Console.WriteLine();
                switch (command)
                {
                    case "quit":
                        flag = false;
                        break;
                    case "one":
                        One();
                        break;
                    case "two":
                        Two();
                        break;
                    case "three":
                        Three();
                        break;
                    case "man":
                        Man();
                        break;
                    default:
                        Console.WriteLine("What? =/");
                        break;
                }
                Console.WriteLine();
            }
        }

        static void Man()
        {
            Console.WriteLine("one - first method");
            Console.WriteLine("two - second method");
            Console.WriteLine("three - third method");
        }

        static void One()
        {
            Console.Write("Enter Id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                var result = client.GetFromJsonAsync<Dictionary<Hierarchy, int>>(path + "LinqQuery/First/" + id).Result;
                foreach (var item in result)
                {
                    Console.WriteLine("Project: " + item.Key.Name);
                    Console.WriteLine("Task Count: " + item.Value);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Error");
            }
        }

        static void Two()
        {
            Console.Write("Enter Id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                var result = client.GetFromJsonAsync<List<Task>>(path + "LinqQuery/Second/" + id).Result;
                foreach (var item in result)
                {
                    Console.WriteLine(item.Name);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Error");
            }
        }

        static void Three()
        {
            Console.Write("Enter Id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                var result = client.GetFromJsonAsync<List<Third>>(path + "LinqQuery/Third/" + id).Result;
                foreach (var item in result)
                {
                    Console.WriteLine(item.id + " " + item.name);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Error");
            }
        }
    }
}
