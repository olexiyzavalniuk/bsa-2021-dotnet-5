﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WebAPI.DTO;
using Microsoft.Extensions.DependencyInjection;

namespace WebAPI
{
    public class WebApiDBcontext : DbContext
    {
        public WebApiDBcontext(DbContextOptions<WebApiDBcontext> options) 
            : base(options)
        {  }

        public DbSet<User> Users { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<DTO.Task> Tasks { get; set; }
        public DbSet<Project> Projects { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            var users = JsonConvert.DeserializeObject<IEnumerable<User>>
                (File.ReadAllText("Data/Users.json")).ToList();
            var teams = JsonConvert.DeserializeObject<IEnumerable<Team>>
                (File.ReadAllText("Data/Teams.json")).ToList();
            var tasks = JsonConvert.DeserializeObject<IEnumerable<DTO.Task>>
                (File.ReadAllText("Data/Tasks.json")).ToList();
            var projects = JsonConvert.DeserializeObject<IEnumerable<Project>>
                (File.ReadAllText("Data/Projects.json")).ToList();

            foreach (var item in users)
            {
                item.Id++;
                item.TeamId++;
            }

            foreach (var item in teams)
            {
                item.Id++;
            }

            foreach (var item in tasks)
            {
                item.Id++;
                item.PerformerId++;
                item.ProjectId++;
            }

            foreach (var item in projects)
            {
                item.Id++;
                item.TeamId++;
                item.AuthorId++;
            }

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<DTO.Task>().HasData(tasks);
            modelBuilder.Entity<Project>().HasData(projects);


            base.OnModelCreating(modelBuilder);
        }
    }
}
