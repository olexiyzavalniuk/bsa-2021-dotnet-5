﻿using System.Collections.Generic;
using System.Linq;
using WebAPI.DTO;

namespace WebAPI.DAL
{
    public class UserRepository : IRepository<User>
    {
        private List<User> _users;

        public UserRepository(IDataGeter<User> dataGeter)
        {
            _users = dataGeter.Get().ToList();
        }

        public void Create(User item)
        {
            _users.Add(item);
        }

        public void Delete(int id)
        {
            var item = _users.Single(i => i.Id == id);
            _users.Remove(item);
        }

        public User Get(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        public void Update(User item)
        {
            Delete(item.Id);
            _users.Add(item);
        }
    }
}
