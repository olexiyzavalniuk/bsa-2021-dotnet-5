﻿using System.Collections.Generic;
using System.Linq;
using WebAPI.DTO;

namespace WebAPI.DAL
{
    public class ProjectRepository : IRepository<Project>
    {
        private List<Project> _projects;

        public ProjectRepository(IDataGeter<Project> dataGeter)
        {
            _projects = dataGeter.Get().ToList();
        }

        public void Create(Project item)
        {
            _projects.Add(item);
        }

        public void Delete(int id)
        {
            var item = _projects.Single(i => i.Id == id);
            _projects.Remove(item);
        }

        public Project Get(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            return _projects;
        }

        public void Update(Project item)
        {
            Delete(item.Id);
            _projects.Add(item);
        }
    }
}
