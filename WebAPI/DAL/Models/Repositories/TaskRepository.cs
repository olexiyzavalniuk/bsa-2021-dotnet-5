﻿using System.Collections.Generic;
using System.Linq;
using WebAPI.DTO;

namespace WebAPI.DAL
{
    public class TaskRepository : IRepository<Task>
    {
        private List<Task> _tasks;

        public TaskRepository(IDataGeter<Task> dataGeter)
        {
            _tasks = dataGeter.Get().ToList();
        }

        public void Create(Task item)
        {
            _tasks.Add(item);
        }

        public void Delete(int id)
        {
            var item = _tasks.Single(i => i.Id == id);
            _tasks.Remove(item);
        }

        public Task Get(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<Task> GetAll()
        {
            return _tasks;
        }

        public void Update(Task item)
        {
            Delete(item.Id);
            _tasks.Add(item);
        }
    }
}
