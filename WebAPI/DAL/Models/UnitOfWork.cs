﻿using System.Collections.Generic;
using System.Linq;
using WebAPI.DTO;

namespace WebAPI.DAL
{
    public class UnitOfWork
    {
        private UserRepository _userRepository;
        private TeamRepository _teamRepository;
        private TaskRepository _taskRepository;
        private ProjectRepository _projectRepository;

        public UserRepository Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(new UsersDataGeter());
                return _userRepository;
            }
        }

        public TeamRepository Teams
        {
            get
            {
                if (_teamRepository == null)
                    _teamRepository = new TeamRepository(new TeamsDataGeter());
                return _teamRepository;
            }
        }

        public TaskRepository Tasks
        {
            get
            {
                if (_taskRepository == null)
                    _taskRepository = new TaskRepository(new TasksDataGeter());
                return _taskRepository;
            }
        }

        public ProjectRepository Projects
        {
            get
            {
                if (_projectRepository == null)
                    _projectRepository = new ProjectRepository(new ProjctsDataGeter());
                return _projectRepository;
            }
        }
    }

}