﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DAL
{
    public interface IDataGeter<T> where T : class
    {
        IEnumerable<T> Get();
    }
}
