using Xunit;
using WebAPI.DTO;
using WebAPI.BLL.Tests.Plugs;
using System.Collections.Generic;

namespace WebAPI.BLL.Tests
{
    public class SecondQueryUnitTest
    {
        [Fact]
        public void RunSecondQuery_When2TasksWith1PerformerIn1Project()
        {
            //Arrange
            var query = new QueryPlug();
            Hierarchy item = new Hierarchy();
            item.Tasks = new List<Task2>();
            User user = new User() { LastName = "Musk", FirstName = "Elon", Id = 8 };
            item.Tasks.Add(new Task2() { Name = "Task1", Performer = user });
            item.Tasks.Add(new Task2() { Name = "Task2", Performer = user });
            query.AddItem(item);

            //ACT
            var result = Second.Run(8, query);

            //ASSERT
            Assert.Equal(2, result.Count);
            Assert.Equal("Elon", result[0].Performer.FirstName);
            Assert.Equal("Musk", result[1].Performer.LastName);
        }

        [Fact]
        public void RunSecondQuery_WhenListIsEmpty()
        {
            //Arrange
            var query = new QueryPlug();
            Hierarchy item = new Hierarchy();
            item.Tasks = new List<Task2>();

            //ACT
            var result = Second.Run(8, query);

            //ASSERT
            Assert.Empty(result);
        }
    }
}
