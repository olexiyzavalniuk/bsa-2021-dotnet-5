﻿using Xunit;
using WebAPI.DTO;
using WebAPI.BLL.Tests.Plugs;
using System.Collections.Generic;
using System;
using WebAPI.DAL;
using System.Linq;

namespace WebAPI.BLL.Tests
{
    public class OtherUnitTests
    {
        [Fact]
        public void CreateUser()
        {
            //Arrange
            var user = new User()
            {
                BirthDay = DateTime.Now,
                Email = "elonmusk@mail.ru",
                FirstName = "Elon",
                LastName = "Musk",
                Id = 8,
                RegisteredAt = DateTime.Now,
                TeamId = 69
            };

            var plug = new UserDataGeterPlug();
            var repository = new UserRepository(plug);

            //Act
            repository.Create(user);
            var actual = repository.GetAll().ToList();

            //Assert
            Assert.Single(actual);
            Assert.Equal(user, actual[0]);

        }

        [Fact]
        public void ChangeTaskStateLikeFinished()
        {
            //Arrange
            var task = new Task()
            {
                Id = 69,
                Name = "Pass this HW",
                State = TaskState.InProgress
            };

            var plug = new TaskDataGeterPlug();
            var repository = new TaskRepository(plug);
            repository.Create(task);

            task.State = TaskState.Done;

            //Act
            repository.Update(task);
            var actual = repository.GetAll().ToList();

            //Assert
            Assert.Single(actual);
            Assert.NotEqual(TaskState.InProgress, actual[0].State);
            Assert.Equal(TaskState.Done, actual[0].State);
        }

        [Fact]
        public void AddUserInTeam()
        {
            //Arrange
            var user = new User()
            {
                BirthDay = DateTime.Now,
                Email = "elonmusk@mail.ru",
                FirstName = "Elon",
                LastName = "Musk",
                Id = 8,
                RegisteredAt = DateTime.Now,
                TeamId = 69
            };

            var plug = new UserDataGeterPlug();
            var repository = new UserRepository(plug);

            //Act
            repository.Create(user);
            var actual = repository.GetAll().ToList();

            //Assert
            Assert.Single(actual);
            Assert.Equal(69, actual[0].TeamId);
        }
    }
}
