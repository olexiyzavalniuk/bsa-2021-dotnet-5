﻿using Xunit;
using WebAPI.DTO;
using WebAPI.BLL.Tests.Plugs;
using System.Collections.Generic;

namespace WebAPI.BLL.Tests
{
    public class FirstQueryUnitTests
    {
        [Fact]
        public void RunFirstQuery_When2TasksWith1Performer()
        {
            //Arrange
            var query = new QueryPlug();
            Hierarchy item = new Hierarchy() { Name = "Viber" };
            item.Tasks = new List<Task2>();
            User user = new User() { LastName = "Musk", FirstName = "Elon", Id = 8 };
            item.Tasks.Add(new Task2() { Name = "Task1", Performer = user });
            item.Tasks.Add(new Task2() { Name = "Task2", Performer = user });
            query.AddItem(item);

            //ACT
            var result = First.Run(8, query);

            //ASSERT
            Assert.Single(result);
            Dictionary<Hierarchy, int> expected = new Dictionary<Hierarchy, int>();
            expected.Add(item, 2);
            Assert.Equal(expected, result);

        }

        [Fact]
        public void RunFirstQuery_WhenListIsEmpty()
        {
            //Arrange
            var query = new QueryPlug();
            Hierarchy item = new Hierarchy();
            item.Tasks = new List<Task2>();

            //ACT
            var result = First.Run(8, query);

            //ASSERT
            Assert.Empty(result);
        }
    }
}
